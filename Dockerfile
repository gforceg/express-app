FROM node
EXPOSE 8080/tcp
COPY . /express-app
WORKDIR /express-app
ENTRYPOINT ["npm", "start"]